/*
	Instrucciones:
	
	Utilizando como base el jercicio en clase, denominado "BC_Personas.pl" agregar lo que
	se pide a continuaci�n:
	
	1. Agregar una regla para identificar a los ABUELOS
	2. Agregar una regla para identificar a las ABUELAS
	3. Agregar una regla para identificar a una PERSONA
	4. Agregar una regla para identificar a un NIETO
	5. Agregar una regla para identificar a una NIETA
	6. Agregar una regla para identificar a los HERMANOS
	7. Agregar una regla para identificar a las HERMANAS
	8. Agregar una regla para identificar a las PAREJAS
	
	Agregar una CONSULTA para probar cada una de las reglas anteriores. Agregarlo como
	comentario para poder verificarlas despu�s.
*/


mujer(ana).
mujer(maria).
mujer(luisa).
mujer(fabiola).
mujer(esther).
mujer(rocio).
mujer(diana).
mujer(vanessa).
mujer(martha).
mujer(romina).

hombre(hugo).
hombre(paco).
hombre(luis).
hombre(mario).
hombre(marco).
hombre(juan).
hombre(pedro).
hombre(raul).
hombre(gabriel).
hombre(sergio).

madre(ana, rocio).
madre(ana, maria).
madre(ana, luis).
madre(martha, diana).
madre(fabiola, romina).
madre(romina, luisa).

padre(raul, rocio).
padre(marco, hugo).
padre(pedro, gabriel).
padre(marco, romina).
padre(sergio, paco).
padre(hugo, luisa).

% *=*=*=*=*=*=*=*=*=*=* REGLAS *=*=*=*=*=*=*=*=*=*=*

% -> Regla para identificar a los ABUELOS
abuelo(X,Y) :- padre(X,Z), padre(Z,Y).
/*
	RESULTADO:
	
	:-abuelo(X,Y).
	true.
	
	X = marco,
	Y = luis :-;
	false.
*/


% -> Regla para identificar a las ABUELAS
abuela(X,Y) :- madre(X,Z), madre(Z,Y).
/*
	RESULTADO:
	
	:-abuela(X,Y).
	true.
	
	X = fabiola,
	Y = luisa :-;
	false.
*/

% -> Regla para identificar a una PERSONA
persona(X) :- mujer(X); hombre(X).
/*
	RESULTADO:
	
	:-persona(X).
	true.
	
	X = ana :-;
	X = maria |;
	X = luisa |;
	X = fabiola |;
	X = esther |;
	X = rocio |;
	X = diana |;
	X = vanessa |;
	X = martha |;
	X = romina |;
	X = hugo |;
	X = paco |;
	X = luis |;
	X = mario |;
	X = marco |;
	X = juan |;
	X = pedro |;
	X = raul |;
	X = gabriel |;
	X = sergio.
*/


% -> Regla para identificar a un NIETO
nieto(X,Y) :- hombre(X), (abuelo(Y,X); abuela(Y,X)).
/*
	RESULTADO:
	
	:-nieto(X,Y).
	true.
	
	X = luis,
	Y = marco :-;
	false.
*/


% -> Regla para identificar a una NIETA
nieta(X,Y) :- mujer(X), (abuelo(Y,X); abuela(Y,X)).
/*
	RESULTADO:
	
	:-nieta(X,Y).
	true.
	
	X = luisa,
	Y = fabiola :-;
	false.
*/


% -> Regla para identificar a los HERMANOS
hermanos(X,Y) :- hombre(X), ((madre(Z,X), padre(D,X)), (madre(Z,Y), padre(D,Y))).
/*
	RESULTADO:
	
	:-hermanos(X,Y).
	true.
*/


% -> Regla para identificar a las HERMANAS
hermanas(X,Y) :- mujer(X), ((madre(Z,X), padre(W,X)), (madre(Z,Y), padre(W,Y))).
/*
	RESULTADO:
	
	:-hermanas(X,Y).
	true.
	
	X = Y, Y = luisa :-;
	X = Y, Y = rocio |;
	X = Y, Y = romina.
*/

% -> Regla para identificar a las PAREJAS
parejas(X,Y) :- padre(X,Z), madre(Y,Z).
/*
	RESULTADO:
	
	:-parejas(X,Y).
	true.
	
	X = raul,
	Y = ana :-;
	X = marco,
	Y = fabiola |;
	X = hugo,
	Y = romina.
*/
